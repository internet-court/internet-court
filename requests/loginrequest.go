package requests

import (
	"encoding/json"
	"gitlab.com/spkvn/internet-court/errors"
	"gitlab.com/spkvn/internet-court/util"
	"net/http"
)

type LoginRequest struct {
	UserName string `json:"username"`
	Password util.Password
	Email    string
}

func DecodeLoginRequest(request *http.Request) (LoginRequest, error) {
	loginRequest := LoginRequest{}
	decoder := json.NewDecoder(request.Body)
	error := decoder.Decode(&loginRequest)
	defer request.Body.Close()
	return loginRequest, error
}

func (logReq LoginRequest) GetIdentifierKeyValue() (string, string, error) {
	key := ""
	val := ""
	if logReq.UserName != "" {
		key = "username"
		val = logReq.UserName
	} else if logReq.Email != "" {
		key = "email"
		val = logReq.Email
	} else {
		return "", "", errors.MissingArgumentError{Message: "Username or Email required"}
	}
	return key, val, nil
}
