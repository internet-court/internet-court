package requests

import (
	"encoding/json"
	"net/http"
)

type VoteRequest struct {
	TrialId uint `json:"trialId"`
	Guilty  bool `json:"guilty"`
}

func DecodeVoteRequest(request *http.Request) (VoteRequest, error) {
	voteRequest := VoteRequest{}
	decoder := json.NewDecoder(request.Body)
	err := decoder.Decode(&voteRequest)
	return voteRequest, err
}
