package requests

import (
	"encoding/json"
	"net/http"
)

type ArgumentRequest struct {
	Argument string `json:"argument"`
}

func DecodeArgumentRequest(request *http.Request) (ArgumentRequest, error) {
	argRequest := ArgumentRequest{}
	decoder := json.NewDecoder(request.Body)
	err := decoder.Decode(&argRequest)
	return argRequest, err
}
