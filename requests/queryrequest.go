package requests

import (
	"fmt"
	"net/http"
	"strconv"
)

func DecodeQueryRequest(request *http.Request) string {
	query := StringFromRequest(request, "query")
	return fmt.Sprintf("%%%s%%", query)
}

func StringFromRequest(request *http.Request, key string) string {
	strings, ok := request.URL.Query()[key]
	if !ok || len(strings[0]) < 1 {
		return ""
	}
	return strings[0]
}

func BoolFromRequest(request *http.Request, key string, defaultBool bool) bool {
	booleanString := StringFromRequest(request, key)
	value, err := strconv.ParseBool(booleanString)
	if err != nil {
		return defaultBool
	}
	return value
}
