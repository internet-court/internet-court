package requests

import (
	"encoding/json"
	"net/http"
)

type TrialRequest struct {
	DefendantId uint     `json:"defendantId"`
	Title       string   `json:"title"`
	Arguments   []string `json:"arguments"`
}

func DecodeTrialRequest(request *http.Request) (TrialRequest, error) {
	trialRequest := TrialRequest{}
	decoder := json.NewDecoder(request.Body)
	err := decoder.Decode(&trialRequest)
	return trialRequest, err
}
