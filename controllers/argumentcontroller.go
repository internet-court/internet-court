package controllers

import (
	"github.com/gorilla/mux"
	"github.com/jinzhu/gorm"
	"gitlab.com/spkvn/internet-court/models"
	"gitlab.com/spkvn/internet-court/requests"
	"net/http"
	"strconv"
)

type ArgumentController struct {
	DB *gorm.DB
}

func (a *ArgumentController) Create(w http.ResponseWriter, r *http.Request) {
	id, err := strconv.Atoi(mux.Vars(r)["id"])
	if err != nil {
		respondError(w, http.StatusBadRequest, "Couldn't parse trial id, expecting digit")
		return
	}
	argRequest, err := requests.DecodeArgumentRequest(r)
	if err != nil {
		respondError(w, http.StatusBadRequest, "Couldn't find argument in request")
	}
	trial, result := models.FirstTrial(id, a.DB)
	if result.RowsAffected == 0 {
		respondError(w, http.StatusNotFound, "No such trial")
		return
	}
	if result.Error != nil {
		respondError(w, http.StatusInternalServerError, "An error occurred")
		return
	}

	userId := uint(getUserIdInt(r))
	if trial.PlaintiffId != userId && trial.DefendantId != userId {
		respondError(w, http.StatusBadRequest, "You can't make this request as you're not a participant")
		return
	}

	argument := models.Argument{
		Text:     argRequest.Argument,
		AuthorId: uint(getUserIdInt(r)),
		TrialId:  trial.ID,
	}

	result = a.DB.Save(&argument)
	if result.Error != nil {
		respondError(w, http.StatusInternalServerError, "Error creating argument")
		return
	}

	respondJson(w, http.StatusCreated, argument)
}
