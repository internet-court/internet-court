package controllers

import (
	"fmt"
	"github.com/jinzhu/gorm"
	"gitlab.com/spkvn/internet-court/models"
	"gitlab.com/spkvn/internet-court/requests"
	"net/http"
)

type AuthController struct {
	DB     *gorm.DB
	JwtKey string
}

func (a *AuthController) Health(w http.ResponseWriter, r *http.Request) {
	respondJson(w, http.StatusOK, `{"health": "up"}`)
}

func (a *AuthController) Register(w http.ResponseWriter, r *http.Request) {
	user, error := models.DecodeUser(r)
	if error != nil {
		respondError(w, http.StatusBadRequest, error.Error())
		return
	}

	if error := a.DB.Save(&user).Error; error != nil {
		respondError(w, http.StatusInternalServerError, error.Error())
		return
	}

	response, error := user.LoginResponse(a.JwtKey)
	if error != nil {
		respondError(w, http.StatusInternalServerError, error.Error())
	}
	respondJson(w, http.StatusCreated, response)
}

func (a *AuthController) LogIn(w http.ResponseWriter, r *http.Request) {
	loginRequest, error := requests.DecodeLoginRequest(r)
	if error != nil {
		respondError(w, http.StatusBadRequest, error.Error())
		return
	}
	key, val, error := loginRequest.GetIdentifierKeyValue()
	if error != nil {
		respondError(w, http.StatusBadRequest, error.Error())
		return
	}

	user := models.User{}
	result := a.DB.Where(fmt.Sprintf("%s = ?", key), val).First(&user)

	if result.Error == nil && loginRequest.Password.CheckAgainstHash(string(user.Password)) {
		// todo: jwt authentication, send token back :-)
		response, error := user.LoginResponse(a.JwtKey)
		if error != nil {
			respondError(w, http.StatusInternalServerError, "Failed to generate token")
			return
		}
		respondJson(w, http.StatusOK, response)
		return
	}
	respondError(w, http.StatusUnauthorized, "Credentials not recognised")
}
