package controllers

import (
	"github.com/aws/aws-sdk-go/aws/session"
	"github.com/gorilla/mux"
	"github.com/jinzhu/gorm"
	"gitlab.com/spkvn/internet-court/config"
	"gitlab.com/spkvn/internet-court/models"
	"gitlab.com/spkvn/internet-court/requests"
	"net/http"
	"strconv"
)

type TrialController struct {
	DB         *gorm.DB
	AWSSession *session.Session
	Config     *config.Config
}

func (t *TrialController) Create(w http.ResponseWriter, r *http.Request) {
	trialRequest, err := requests.DecodeTrialRequest(r)
	if err != nil {
		respondError(w, http.StatusBadRequest, err.Error())
	}
	userId := uint(getUserIdInt(r))
	trial, err := t.saveTrial(trialRequest, userId)
	if err != nil {
		respondError(w, http.StatusInternalServerError, err.Error())
		return
	}
	_, err = t.saveArguments(trialRequest, trial, userId)
	if err != nil {
		respondError(w, http.StatusInternalServerError, err.Error())
		return
	}
	respondJson(w, http.StatusCreated, trial)
}

func (t *TrialController) Search(w http.ResponseWriter, r *http.Request) {
	queryString := requests.DecodeQueryRequest(r)
	userOnly := requests.BoolFromRequest(r, "userOnly", false)
	var trials []models.Trial
	var result *gorm.DB
	if userOnly {
		trials, result = models.SearchOwnTrials(queryString, uint(getUserIdInt(r)), t.DB)
	} else {
		trials, result = models.SearchTrials(queryString, t.DB)
	}
	if result.Error == nil {
		respondJson(w, http.StatusOK, trials)
		return
	}
	respondError(w, http.StatusInternalServerError, "Failed to search trials")
	return
}

func (t *TrialController) Retrieve(w http.ResponseWriter, r *http.Request) {
	params := mux.Vars(r)
	id, err := strconv.Atoi(params["id"])
	userId := getUserIdInt(r)
	if err != nil {
		respondError(w, http.StatusBadRequest, "Couldn't parse a trial id in request")
	}
	trial, result := models.FirstTrialWithUserVotes(id, userId, t.DB)
	if result.RowsAffected == 0 {
		respondError(w, http.StatusNotFound, "No such trial")
	}
	if result.Error != nil {
		respondError(w, http.StatusInternalServerError, "An error occurred")
	}
	trial.Plaintiff.ProfilePicture.RetreivePublicUrl(t.AWSSession, t.Config.BucketName)
	trial.Defendant.ProfilePicture.RetreivePublicUrl(t.AWSSession, t.Config.BucketName)
	respondJson(w, http.StatusOK, trial)
}

func (t *TrialController) RetrieveVotes(w http.ResponseWriter, r *http.Request) {
	params := mux.Vars(r)
	id, err := strconv.Atoi(params["id"])
	if err != nil {
		respondError(w, http.StatusBadRequest, "Couldn't parse a trial id in request")
	}
	votes, result := models.AllVotesByTrial(id, t.DB)
	if result.RowsAffected == 0 {
		respondError(w, http.StatusNotFound, "Failed to find Trial or Votes")
	}
	if result.Error != nil {
		respondError(w, http.StatusInternalServerError, "An error occurred")
	}
	respondJson(w, http.StatusOK, votes)
}

func (t *TrialController) saveTrial(request requests.TrialRequest, userId uint) (models.Trial, error) {
	trial := models.Trial{
		Title:       request.Title,
		PlaintiffId: userId,
		DefendantId: request.DefendantId,
	}
	err := t.DB.Save(&trial).Error
	return trial, err
}

func (t *TrialController) saveArguments(request requests.TrialRequest, trial models.Trial, userId uint) ([]models.Argument, error) {
	arguments := make([]models.Argument, 0)
	err := error(nil)
	for _, arg := range request.Arguments {
		argument := models.Argument{AuthorId: userId, Text: arg, TrialId: trial.ID}
		err = t.DB.Save(&argument).Error
		arguments = append(arguments, argument)
		if err != nil {
			return arguments, err
		}
	}
	return arguments, err
}
