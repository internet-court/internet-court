package controllers

import (
	"fmt"
	"github.com/jinzhu/gorm"
	"gitlab.com/spkvn/internet-court/models"
	"gitlab.com/spkvn/internet-court/requests"
	"net/http"
)

type VoteController struct {
	DB *gorm.DB
}

func (v *VoteController) Create(w http.ResponseWriter, r *http.Request) {
	voteRequest, err := requests.DecodeVoteRequest(r)
	if err != nil {
		respondError(w, http.StatusBadRequest, "Failed to parse vote?")
		return
	}

	voterId := uint(getUserIdInt(r))
	hasVotedBefore, vote := models.HasUserVotedBefore(voteRequest.TrialId, voterId, v.DB)
	trial := models.Trial{}
	query := v.DB.Find(&trial, voteRequest.TrialId)

	if query.RecordNotFound() {
		message := fmt.Sprintf("Failed to find trial with id: %d", voteRequest.TrialId)
		respondError(w, http.StatusNotFound, message)
		return
	}

	if trial.PlaintiffId == voterId || trial.DefendantId == voterId {
		message := "The voter is also a participant in the trial! Sneaky!"
		respondError(w, http.StatusBadRequest, message)
		return
	}

	if hasVotedBefore {
		vote.Guilty = voteRequest.Guilty
	} else {
		vote = models.Vote{
			VoterId: voterId,
			TrialId: voteRequest.TrialId,
			Guilty:  voteRequest.Guilty,
		}
	}
	v.DB.Save(&vote)

	if v.DB.Error != nil {
		respondError(w, http.StatusInternalServerError, "Failed to save vote :(")
		return
	}
	respondJson(w, http.StatusCreated, vote)
}
