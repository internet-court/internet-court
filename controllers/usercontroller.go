package controllers

import (
	"github.com/aws/aws-sdk-go/aws/session"
	"github.com/jinzhu/gorm"
	"gitlab.com/spkvn/internet-court/attachments"
	"gitlab.com/spkvn/internet-court/config"
	"gitlab.com/spkvn/internet-court/models"
	"gitlab.com/spkvn/internet-court/requests"
	"net/http"
)

type UserController struct {
	DB         *gorm.DB
	AWSSession *session.Session
	Config     *config.Config
}

func (u *UserController) Retrieve(w http.ResponseWriter, r *http.Request) {
	queryString := requests.DecodeQueryRequest(r)
	userId := getUserId(r)
	users, result := models.SearchUsersExcluding(queryString, userId, u.DB)
	if result.Error == nil {
		respondJson(w, http.StatusOK, users)
		return
	}
	respondError(w, http.StatusInternalServerError, "Failed to search users")
}

func (u *UserController) RetrieveSelf(w http.ResponseWriter, r *http.Request) {
	userId := getUserId(r)
	user := models.User{}
	result := u.DB.
		Preload("ProfilePicture").
		Where("id = ?", userId).
		First(&user)
	if result.RowsAffected == 0 {
		respondError(w, http.StatusNotFound, "Failed to find user")
		return
	}
	user.ProfilePicture.RetreivePublicUrl(u.AWSSession, u.Config.BucketName)
	if result.Error != nil {
		respondError(w, http.StatusInternalServerError, result.Error.Error())
	}
	respondJson(w, http.StatusOK, user)
}

func (u *UserController) SaveSelf(w http.ResponseWriter, r *http.Request) {
	userId := getUserId(r)
	user := models.User{}
	result := u.DB.Where("id = ?", userId).First(&user)
	if result.RowsAffected == 0 {
		respondError(w, http.StatusNotFound, "Failed to find user")
		return
	}
	if result.Error != nil {
		respondError(w, http.StatusInternalServerError, result.Error.Error())
		return
	}

	err := r.ParseMultipartForm(maxUpload)
	if err != nil {
		respondError(w, http.StatusBadRequest, err.Error())
		return
	}

	user.UserName = r.FormValue("username")
	user.FirstName = r.FormValue("first_name")
	user.LastName = r.FormValue("last_name")
	user.Email = r.FormValue("email")

	// Save imageFile
	imageFile, err := attachments.CreateImageFile(r, "file")
	if err != nil {
		respondError(w, http.StatusInternalServerError, err.Error())
		return
	}
	url, err := imageFile.SaveToS3(u.AWSSession, u.Config.BucketName)
	if err != nil {
		respondError(w, http.StatusInternalServerError, err.Error())
		return
	}
	image := models.CreateImage(imageFile, url)

	result = u.DB.Save(&image)
	if result.Error != nil {
		respondError(w, http.StatusInternalServerError, result.Error.Error())
		return
	}
	user.ProfilePictureId = image.ID

	result = u.DB.Save(&user)
	if result.Error != nil {
		respondError(w, http.StatusInternalServerError, result.Error.Error())
		return
	}

	// searching again, after loading.
	result = u.DB.Preload("ProfilePicture").Find(&user)
	if result.Error != nil {
		respondError(w, http.StatusInternalServerError, result.Error.Error())
		return
	}
	user.ProfilePicture.RetreivePublicUrl(u.AWSSession, u.Config.BucketName)

	respondJson(w, http.StatusOK, user)
}
