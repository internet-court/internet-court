package controllers

import (
	"encoding/json"
	"github.com/dgrijalva/jwt-go"
	"net/http"
	"strconv"
)

// 32 MB
const maxUpload = 32 * 1024 * 1024

func respondJson(w http.ResponseWriter, status int, payload interface{}) {
	response, error := json.Marshal(payload)
	if error != nil {
		w.WriteHeader(http.StatusInternalServerError)
		w.Write([]byte(error.Error()))
		return
	}
	w.Header().Set("Content-Type", "application/json")
	w.WriteHeader(status)
	w.Write([]byte(response))
}

func respondError(w http.ResponseWriter, status int, message string) {
	respondJson(w, status, map[string]string{"error": message})
}

func getUserId(r *http.Request) string {
	userId := strconv.Itoa(getUserIdInt(r))
	return userId
}

func getUserIdInt(r *http.Request) int {
	return int(r.Context().Value("user").(*jwt.Token).
		Claims.(jwt.MapClaims)["user"].(float64))
}
