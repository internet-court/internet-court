module gitlab.com/spkvn/internet-court

go 1.13

require (
	github.com/auth0/go-jwt-middleware v0.0.0-20190805220309-36081240882b
	github.com/aws/aws-sdk-go v1.35.2
	github.com/dgrijalva/jwt-go v3.2.0+incompatible
	github.com/gorilla/mux v1.7.3
	github.com/jinzhu/gorm v1.9.12
	github.com/rs/cors v1.7.0
	golang.org/x/crypto v0.0.0-20191205180655-e7c4368fe9dd
)
