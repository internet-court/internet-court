package config

import (
	"fmt"
	"github.com/aws/aws-sdk-go/aws"
	"github.com/aws/aws-sdk-go/aws/credentials"
	"github.com/aws/aws-sdk-go/aws/session"
	"github.com/jinzhu/gorm"
)

type Config struct {
	DB         *DBConfig
	AWS        *aws.Config
	BucketName string
	JwtKey     string
}

type DBConfig struct {
	Dialect  string
	Username string
	Password string
	Name     string
	Charset  string
}

func GetConfig() *Config {
	return &Config{
		JwtKey: "kevin_has_a_big_stinky_ass",
		AWS: &aws.Config{
			Region: aws.String("ap-southeast-2"),
			Credentials: credentials.NewStaticCredentials(
				"AKIAXIDXHTKQHAJWSESR",
				"z/GmwvpGhyb+PhtMbvhTZA55OGj3BppxxI23dxpk",
				""),
		},
		BucketName: "internet-court-develop",
		DB: &DBConfig{
			Dialect:  "mysql",
			Username: "fuscus-go",
			Name:     "fuscus-go",
			Password: "money",
			Charset:  "utf8",
		},
	}
}

func (config Config) GetDatabase() (*gorm.DB, error) {
	dbURI := fmt.Sprintf("%s:%s@/%s?charset=%s&parseTime=True",
		config.DB.Username,
		config.DB.Password,
		config.DB.Name,
		config.DB.Charset)
	db, err := gorm.Open(config.DB.Dialect, dbURI)
	return db, err
}

func (config Config) GetAwsSession() (*session.Session, error) {
	return session.NewSession(config.AWS)
}
