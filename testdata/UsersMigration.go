package testdata

import (
	"github.com/jinzhu/gorm"
	"gitlab.com/spkvn/internet-court/models"
	"math/rand"
)

type UsersMigration struct {
	Users []models.User `json:users`
}

func (u UsersMigration) Process(testUserUsername string, db *gorm.DB) models.User {
	var testUser models.User
	for i, user := range u.Users {
		db.Save(&user)
		u.Users[i] = user
		if user.UserName == testUserUsername {
			testUser = user
		}
	}
	return testUser
}

func (u UsersMigration) randomUserPair() (models.User, models.User) {
	var plaintiff, defendant models.User
	for (plaintiff.ID == 0 && defendant.ID == 0) || defendant.ID == plaintiff.ID {
		plaintiff = u.Users[rand.Intn(len(u.Users))]
		defendant = u.Users[rand.Intn(len(u.Users))]
	}
	return plaintiff, defendant
}

func (u UsersMigration) RandomUser(exceptId uint) models.User {
	var selected models.User
	for selected.ID == 0 || selected.ID == exceptId {
		selected = u.Users[rand.Intn(len(u.Users))]
	}
	return selected
}

func (u UsersMigration) contains(search []uint, item uint) bool {
	for _, el := range search {
		if el == item {
			return true
		}
	}
	return false
}

func (u UsersMigration) RandomUserExcept(exceptIds []uint) models.User {
	var selected models.User
	for selected.ID == 0 || u.contains(exceptIds, selected.ID) {
		selected = u.Users[rand.Intn(len(u.Users))]
	}
	return selected
}
