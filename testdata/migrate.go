package testdata

import (
	"encoding/json"
	"fmt"
	"github.com/jinzhu/gorm"
	"gitlab.com/spkvn/internet-court/models"
	"io/ioutil"
	"os"
)

func FillDatabaseFromFile(db *gorm.DB, path string) {
	file, err := os.Open(path)
	if err != nil {
		fmt.Println(err.Error())
		return
	}
	defer file.Close()
	bytes, ioerr := ioutil.ReadAll(file)
	if ioerr != nil {
		fmt.Println(ioerr.Error())
		return
	}

	// empty existing db
	fmt.Println("Deleting Tables")
	EmptyTables(db)

	fmt.Println("Inserting Data")
	// process users
	var users UsersMigration
	var kevin models.User
	json.Unmarshal(bytes, &users)
	kevin = users.Process("spkvn", db)

	// process trials
	var trials TrialsMigration
	json.Unmarshal(bytes, &trials)
	trials.Process(kevin, users, 20, db)
}

func EmptyTables(db *gorm.DB) {
	db.Exec("SET FOREIGN_KEY_CHECKS = 0")
	db.Exec("TRUNCATE TABLE votes")
	db.Exec("TRUNCATE TABLE arguments")
	db.Exec("TRUNCATE TABLE trials")
	db.Exec("TRUNCATE TABLE users")
	db.Exec("SET FOREIGN_KEY_CHECKS = 1")
}
