package testdata

import (
	"fmt"
	"github.com/jinzhu/gorm"
	"gitlab.com/spkvn/internet-court/models"
	"math/rand"
)

type TrialsMigration struct {
	Trials []models.Trial `json:trials`
}

func (t TrialsMigration) Process(testUser models.User, users UsersMigration, numberOfVotes int, db *gorm.DB) {
	for index, trial := range t.Trials {
		if index == 0 {
			trial.PlaintiffId = testUser.ID
			trial.DefendantId = users.RandomUser(testUser.ID).ID
		} else {
			plaintiff, defendant := users.randomUserPair()
			trial.PlaintiffId = plaintiff.ID
			trial.DefendantId = defendant.ID
		}
		for index, argument := range trial.Arguments {
			if index%2 == 0 {
				argument.AuthorId = trial.PlaintiffId
			} else {
				argument.AuthorId = trial.DefendantId
			}
			trial.Arguments[index] = argument
		}
		for i := 0; i < numberOfVotes; i++ {
			voter := users.RandomUserExcept([]uint{trial.DefendantId, trial.PlaintiffId})
			vote := models.Vote{
				VoterId: voter.ID,
				Guilty:  rand.Intn(2) != 0,
			}
			trial.Votes = append(trial.Votes, vote)
		}
		fmt.Printf("title: %s,\n", trial.Title)
		fmt.Printf("plaintiff: %d,\n", trial.PlaintiffId)
		fmt.Printf("defendant: %d,\n", trial.DefendantId)
		fmt.Printf("expiry: %s,\n", trial.Expiry)
		fmt.Printf("arguments: %d,\n", len(trial.Arguments))
		fmt.Printf("votes: %d,\n", len(trial.Votes))
		if index != len(t.Trials)-1 {
			fmt.Printf("---------------\n")
		}
		db.Save(&trial)
	}
}
