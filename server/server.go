package server

import (
	"github.com/aws/aws-sdk-go/aws/session"
	"github.com/gorilla/mux"
	"github.com/jinzhu/gorm"
	"github.com/rs/cors"
	"gitlab.com/spkvn/internet-court/config"
	"gitlab.com/spkvn/internet-court/controllers"
	"gitlab.com/spkvn/internet-court/middleware"
	"gitlab.com/spkvn/internet-court/models"
	"log"
	"net/http"
)

type Server struct {
	Router     *mux.Router
	DB         *gorm.DB
	AWSSession *session.Session
	Config     *config.Config
}

func (s *Server) SetUpRouter() {
	// controller initalisation
	auth := &controllers.AuthController{s.DB, s.Config.JwtKey}
	trial := &controllers.TrialController{s.DB, s.AWSSession, s.Config}
	user := &controllers.UserController{s.DB, s.AWSSession, s.Config}
	argument := &controllers.ArgumentController{s.DB}
	vote := &controllers.VoteController{s.DB}

	// middleware
	authMiddleware := middleware.AuthMiddleware{JwtKey: s.Config.JwtKey}

	// public routes
	s.Router.HandleFunc("/", auth.Health).Methods("GET")
	s.Router.HandleFunc("/register", auth.Register).Methods("POST")
	s.Router.HandleFunc("/login", auth.LogIn).Methods("POST")

	// private routes
	api := s.Router.PathPrefix("/api").Subrouter()
	api.HandleFunc("/trial", trial.Search).Methods("GET")
	api.HandleFunc("/trial/{id}", trial.Retrieve).Methods("GET")
	api.HandleFunc("/trial/{id}/votes", trial.RetrieveVotes).Methods("GET")
	api.HandleFunc("/trial/{id}/argument", argument.Create).Methods("POST")
	api.HandleFunc("/trial", trial.Create).Methods("POST")
	api.HandleFunc("/vote", vote.Create).Methods("POST")
	api.HandleFunc("/user", user.Retrieve).Methods("GET")
	api.HandleFunc("/self", user.SaveSelf).Methods("POST")
	api.HandleFunc("/self", user.RetrieveSelf).Methods("GET")
	api.Use(authMiddleware.Protect)
}

func (s *Server) Initialise(config *config.Config) error {
	db, dbError := config.GetDatabase()
	if dbError != nil {
		log.Fatalf("Failed to connect to DB: %s", dbError.Error())
		return dbError
	}
	awsSession, awsError := config.GetAwsSession()
	if awsError != nil {
		log.Fatalf("Failed to create AWS session: %s", awsError.Error())
		return awsError
	}
	s.AWSSession = awsSession
	s.DB = models.DBMigrate(db)
	s.DB.LogMode(true)
	s.Config = config
	s.Router = mux.NewRouter()
	s.SetUpRouter()
	return nil
}

// Run the app on it's router
func (s *Server) Run(host string) {
	corsConfig := cors.New(cors.Options{
		AllowedOrigins: []string{"*"},
		AllowedMethods: []string{
			http.MethodGet,
			http.MethodPut,
			http.MethodPost,
			http.MethodDelete,
		},
		AllowedHeaders: []string{"*"},
	})

	log.Fatal(http.ListenAndServe(host, corsConfig.Handler(s.Router)))
}
