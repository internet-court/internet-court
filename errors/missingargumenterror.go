package errors

import "fmt"

type MissingArgumentError struct {
	Message string
}

func (mae MissingArgumentError) Error() string {
	return fmt.Sprintf("MissingArgumentError: %s", mae.Message)
}
