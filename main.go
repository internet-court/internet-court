package main

import (
	"fmt"
	"gitlab.com/spkvn/internet-court/config"
	"gitlab.com/spkvn/internet-court/server"
	"gitlab.com/spkvn/internet-court/testdata"
	"os"
)

func serve() {
	fmt.Println("Initializing internet-court server :)")
	configuration := config.GetConfig()
	internetCourtServer := &server.Server{}
	initError := internetCourtServer.Initialise(configuration)
	if initError != nil {
		return
	}
	internetCourtServer.Run(":6969") // lol
}

func fillDb() {
	fmt.Println("Filling database with test data :)")
	db, err := config.GetConfig().GetDatabase()
	if err != nil {
		fmt.Printf("Failed to connect to DB: %s\n", err.Error())
		return
	}
	testdata.FillDatabaseFromFile(db, "./resources/test-database.json")
}

func usage() {
	fmt.Printf("%s {fill-db|serve}\n", os.Args[0])
	fmt.Println("-----------------")
	fmt.Println("Internet court web server.")
}

func main() {
	if len(os.Args) == 1 {
		usage()
	} else {
		switch os.Args[1] {
		case "fill-db":
			fillDb()
			break
		case "serve":
			serve()
			break
		default:
			usage()
		}
	}
}
