package attachments

import (
	"bytes"
	"errors"
	"fmt"
	"github.com/aws/aws-sdk-go/aws"
	"github.com/aws/aws-sdk-go/aws/session"
	"github.com/aws/aws-sdk-go/service/s3"
	"log"
	"mime/multipart"
	"net/http"
	"strings"
	"time"
)

type ImageFile struct {
	Header *multipart.FileHeader
	File   multipart.File
}

func CreateImageFile(r *http.Request, name string) (ImageFile, error) {
	file, header, err := r.FormFile(name)
	image := ImageFile{}
	if err != nil {
		return image, err
	}
	image.File = file
	image.Header = header
	if !image.isCorrectFileType() {
		err = errors.New("incorrect file type")
	}
	return image, err
}

func (i ImageFile) isCorrectFileType() bool {
	name := i.Header.Filename
	types := []string{".gif", ".png", ".jpg", ".jpeg"}
	for _, fileType := range types {
		if strings.Contains(name, fileType) {
			return true
		}
	}
	log.Printf("Not acceptable image: %s\n", name)
	return false
}

func (i ImageFile) GetType() string {
	fileNameParts := strings.Split(i.Header.Filename, ".")
	return fileNameParts[len(fileNameParts)-1]
}

func (i ImageFile) SaveToS3(s *session.Session, bucketName string) (string, error) {
	defer i.File.Close()
	buffer := make([]byte, i.Header.Size)
	i.File.Read(buffer)
	now := time.Now().Format("2006-01-02_15:04:05")
	fileName := fmt.Sprintf("%s_%s", i.Header.Filename, now)
	putObjectInput := s3.PutObjectInput{
		Bucket:        &bucketName,
		Key:           &fileName,
		Body:          bytes.NewReader(buffer),
		ContentLength: &i.Header.Size,
		ContentType:   aws.String(http.DetectContentType(buffer)),
	}
	_, err := s3.New(s).PutObject(&putObjectInput)
	return fileName, err
}
