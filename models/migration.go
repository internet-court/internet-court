package models

import (
	"github.com/jinzhu/gorm"
)

// DBMigrate will create and migrate the tables, and then make the some relationships if necessary
func DBMigrate(db *gorm.DB) *gorm.DB {
	db.AutoMigrate(&User{})
	db.AutoMigrate(&Image{})
	db.AutoMigrate(&Trial{})
	db.AutoMigrate(&Argument{})
	db.AutoMigrate(&Vote{})
	db.Model(&Trial{}).AddForeignKey("plaintiff_id", "users(id)", "RESTRICT", "RESTRICT")
	db.Model(&Trial{}).AddForeignKey("defendant_id", "users(id)", "RESTRICT", "RESTRICT")
	db.Model(&Argument{}).AddForeignKey("trial_id", "trials(id)", "RESTRICT", "RESTRICT")
	db.Model(&Argument{}).AddForeignKey("author_id", "users(id)", "RESTRICT", "RESTRICT")
	db.Model(&Vote{}).AddForeignKey("trial_id", "trials(id)", "RESTRICT", "RESTRICT")
	db.Model(&Vote{}).AddForeignKey("voter_id", "users(id)", "RESTRICT", "RESTRICT")
	db.Model(&User{}).AddForeignKey("profile_picture_id", "images(id)", "RESTRICT", "RESTRICT")
	return db
}
