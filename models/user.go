package models

import (
	"encoding/json"
	"github.com/dgrijalva/jwt-go"
	"github.com/jinzhu/gorm"
	_ "github.com/jinzhu/gorm/dialects/mysql"
	"gitlab.com/spkvn/internet-court/util"
	"log"
	"net/http"
	"time"
)

type User struct {
	gorm.Model
	FirstName        string `json:"first_name"`
	LastName         string `json:"last_name"`
	UserName         string `gorm:"unique;not null;column:username",json:"username"`
	Email            string `gorm:"unique;not null",json:"email"`
	ProfilePictureId uint   `gorm:"column:profile_picture_id"`
	ProfilePicture   Image
	Password         util.Password
}

func (u *User) Hash() string {
	hashedPass, error := u.Password.Hash()
	if error != nil {
		log.Printf("Error hashing pass: %s\n", error)
	}
	return hashedPass
}

func (u *User) BeforeCreate(scope *gorm.Scope) error {
	scope.SetColumn("password", u.Hash())
	return nil
}

func (u *User) LoginResponse(jwtKey string) (map[string]interface{}, error) {
	response := make(map[string]interface{})
	response["user"] = u
	token := jwt.NewWithClaims(jwt.SigningMethodHS256, jwt.MapClaims{
		"user": u.ID,
		"exp":  time.Now().Add(time.Hour * time.Duration(12)).Unix(),
		"iat":  time.Now().Unix(),
	})
	tokenString, err := token.SignedString([]byte(jwtKey))
	response["token"] = tokenString
	return response, err
}

func DecodeUser(request *http.Request) (User, error) {
	user := User{}
	decoder := json.NewDecoder(request.Body)
	error := decoder.Decode(&user)
	defer request.Body.Close()
	return user, error
}

func SearchUsers(q string, DB *gorm.DB) ([]User, *gorm.DB) {
	users := make([]User, 0)
	if q != "" {
		result := DB.
			Where("username LIKE ?", q).
			Find(&users)
		return users, result
	} else {
		result := DB.Find(&users)
		return users, result
	}
}

func SearchUsersExcluding(q, excluding string, DB *gorm.DB) ([]User, *gorm.DB) {
	users := make([]User, 0)
	if q != "" {
		result := DB.
			Not("id", excluding).
			Where("username LIKE ?", q).
			Find(&users)
		return users, result
	} else {
		result := DB.Not("id", excluding).Find(&users)
		return users, result
	}
}
