package models

import "github.com/jinzhu/gorm"

type Vote struct {
	gorm.Model
	Trial   Trial
	TrialId uint
	Voter   User
	VoterId uint
	Guilty  bool
}

func HasUserVotedBefore(trialId uint, voterId uint, DB *gorm.DB) (bool, Vote) {
	vote := Vote{}
	query := DB.Where("trial_id = ?", trialId).
		Where("voter_id = ?", voterId).
		First(&vote)
	return !query.RecordNotFound(), vote
}

func AllVotesByTrial(trialId int, DB *gorm.DB) ([]Vote, *gorm.DB) {
	votes := make([]Vote, 0)
	db := DB.Where("trial_id = ?", trialId).Find(&votes)
	return votes, db
}
