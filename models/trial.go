package models

import (
	"encoding/json"
	"fmt"
	"github.com/jinzhu/gorm"
	_ "github.com/jinzhu/gorm/dialects/mysql"
	"net/http"
	"time"
)

type Trial struct {
	gorm.Model
	Title       string
	Plaintiff   User
	Defendant   User
	PlaintiffId uint
	DefendantId uint
	Expiry      time.Time
	Arguments   []Argument
	Votes       []Vote
}

const TrialPlaintiffJoin string = "INNER JOIN users AS plaintiff ON plaintiff.id = trials.plaintiff_id"
const TrialDefendantJoin string = "INNER JOIN users AS defendant ON defendant.id = trials.defendant_id"

func DecodeTrial(request *http.Request) (Trial, error) {
	trial := Trial{}
	decoder := json.NewDecoder(request.Body)
	error := decoder.Decode(&trial)
	defer request.Body.Close()
	return trial, error
}

func (t Trial) BeforeCreate(scope *gorm.Scope) error {
	hours := 24 * 14
	duration, _ := time.ParseDuration(fmt.Sprintf("%dh", hours))
	scope.SetColumn("expiry", time.Now().Add(duration))
	return nil
}

func SearchTrials(q string, DB *gorm.DB) ([]Trial, *gorm.DB) {
	trials := make([]Trial, 0)
	if q != "%%" {
		result := DB.
			Preload("Plaintiff").
			Preload("Defendant").
			Where("title LIKE ?", q).
			Or("plaintiff.username LIKE ?", q).
			Or("defendant.username LIKE ?", q).
			Joins(TrialPlaintiffJoin). // do I need to use .Joins(STR) ?
			Joins(TrialDefendantJoin).
			Find(&trials)
		return trials, result
	} else {
		result := DB.Preload("Plaintiff").Preload("Defendant").Find(&trials)
		return trials, result
	}
}

func FirstTrial(trialId int, DB *gorm.DB) (Trial, *gorm.DB) {
	trial := Trial{}
	result := DB.
		Preload("Plaintiff").
		Preload("Defendant").
		Preload("Arguments").
		Preload("Arguments.Author").
		First(&trial, trialId)
	return trial, result
}

func FirstTrialWithUserVotes(trialId, userId int, DB *gorm.DB) (Trial, *gorm.DB) {
	trial := Trial{}
	result := DB.
		Preload("Plaintiff").
		Preload("Plaintiff.ProfilePicture").
		Preload("Defendant").
		Preload("Defendant.ProfilePicture").
		Preload("Arguments").
		Preload("Arguments.Author").
		Preload("Votes", "voter_id = ?", userId).
		First(&trial, trialId)
	return trial, result
}

func SearchOwnTrials(q string, userId uint, DB *gorm.DB) ([]Trial, *gorm.DB) {
	trials := make([]Trial, 0)
	var result *gorm.DB
	if q != "%%" {
		result = DB.
			Preload("Plaintiff").
			Preload("Defendant").
			Where("plaintiff_id = ? OR defendant_id = ?", userId, userId).
			Where("title LIKE ? OR plaintiff.username LIKE ? OR defendant.username LIKE ?", q, q, q).
			Joins(TrialPlaintiffJoin).
			Joins(TrialDefendantJoin).
			Find(&trials)
	} else {
		result = DB.
			Preload("Plaintiff").
			Preload("Defendant").
			Where("plaintiff_id = ? OR defendant_id = ?", userId, userId).
			Find(&trials)
	}
	return trials, result
}
