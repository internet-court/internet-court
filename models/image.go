package models

import (
	"github.com/aws/aws-sdk-go/aws/session"
	"github.com/aws/aws-sdk-go/service/s3"
	"github.com/jinzhu/gorm"
	"gitlab.com/spkvn/internet-court/attachments"
	"log"
	"time"
)

type Image struct {
	gorm.Model
	Type      string
	FileName  string
	PublicUrl string `gorm:"-"`
}

func CreateImage(file attachments.ImageFile, url string) Image {
	i := Image{}
	i.Type = file.GetType()
	i.FileName = url
	return i
}

func (i *Image) RetreivePublicUrl(s *session.Session, bucket string) {
	s3Service := s3.New(s)
	getObjectInput := s3.GetObjectInput{
		Bucket: &bucket,
		Key:    &i.FileName,
	}

	req, _ := s3Service.GetObjectRequest(&getObjectInput)
	url, err := req.Presign(60 * 24 * time.Minute)
	if err != nil {
		log.Printf("Failed to presign image")
		// set to default image url
		i.PublicUrl = "http://placekitten.com/200/200"
	}
	i.PublicUrl = url
}
