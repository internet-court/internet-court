package models

import (
	"github.com/jinzhu/gorm"
)

type Argument struct {
	gorm.Model
	Text     string
	Author   User
	Trial    Trial
	AuthorId uint
	TrialId  uint
}

//const ArgumentTrialJoin string = "INNER JOIN trials ON trials.id = argument.trial_id"
//const ArgumentAuthorJoin string = "INNER JOIN users as author ON author.id = argument.author_id"
